curl -sL https://rpm.nodesource.com/setup_4.x | bash -
yum install -y nodejs --disablerepo=epel
npm install npm -g

yum install -y php-pdo php-mysql

apachectl restart

yum install -y composer

yum install -y http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm
yum install -y mysql-community-server
systemctl start mysqld
systemctl enable mysqld.service
mysql_upgrade